package com.atlassian.fastdev.pageobjects;

import org.openqa.selenium.WebElement;

public class Task
{
    private final WebElement taskElement;

    public Task(WebElement taskElement)
    {
        this.taskElement = taskElement;
    }

    public boolean isSuccessful()
    {
        return taskElement.getAttribute("class").contains("successful");
    }
}
