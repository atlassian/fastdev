package com.atlassian.fastdev;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.fastdev.util.Option.none;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestAutoReloadFilter
{
    private static final String GET = "GET";
    private static final String POST = "POST";
    private static final String JSPA_URI = "http://localhost:2990/secure/Dashboard.jspa";
    private static final String ACTION_URI = "http://localhost:2990/login.action";
    private static final String SOME_BROWSER = "Some Browser";
    private static final String CACHE_CONTROL = "Cache-Control";
    private static final String NO_CACHE = "no-cache";
    private static final String PRAGMA = "Pragma";
    private static final String USER_AGENT = "User-Agent";
    private static final String NON_SERVLET_URI = "http://localhost:2990/images/icon.png";
    private static final String SERVLET_URI = "http://localhost:2990/plugins/servlet/test";

    @Mock ReloadHandler reloadHandler;
    @Mock HttpServletRequest request;
    @Mock ServletResponse response;
    @Mock FilterChain chain;

    private AutoReloadFilter autoReloadFilter;

    @Before
    public void setUp() throws Exception
    {
        autoReloadFilter = new AutoReloadFilter(reloadHandler);
        when(reloadHandler.reloadPlugins()).thenReturn(none(String.class));

        // Set up a standard request
        when(request.getMethod()).thenReturn(GET);
        when(request.getRequestURI()).thenReturn(SERVLET_URI);
        when(request.getHeader(PRAGMA)).thenReturn(NO_CACHE);
        when(request.getHeader(CACHE_CONTROL)).thenReturn(NO_CACHE);
        when(request.getHeader(USER_AGENT)).thenReturn(SOME_BROWSER);
    }

    @Test
    public void assertThatReloadIsTriggeredByNoCacheHeaders() throws IOException, ServletException
    {
        autoReloadFilter.doFilter(request, response, chain);

        verify(reloadHandler).reloadPlugins();
    }

    @Test
    public void assertThatReloadIsTriggeredOnActionRequest() throws IOException, ServletException
    {
        when(request.getRequestURI()).thenReturn(ACTION_URI);

        autoReloadFilter.doFilter(request, response, chain);

        verify(reloadHandler).reloadPlugins();
    }

    @Test
    public void assertThatReloadIsTriggeredOnJspaRequest() throws IOException, ServletException
    {
        when(request.getRequestURI()).thenReturn(JSPA_URI);

        autoReloadFilter.doFilter(request, response, chain);

        verify(reloadHandler).reloadPlugins();
    }

    @Test
    public void assertThatReloadIsTriggeredForMSIEWithOnlyCacheControlHeader() throws IOException, ServletException
    {
        when(request.getHeader(PRAGMA)).thenReturn(null);
        when(request.getHeader(USER_AGENT)).thenReturn("Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)");

        autoReloadFilter.doFilter(request, response, chain);

        verify(reloadHandler).reloadPlugins();
    }

    @Test
    public void assertThatReloadIsNotTriggeredByHeaderlessRequest() throws IOException, ServletException
    {
        when(request.getRequestURI()).thenReturn(SERVLET_URI);
        when(request.getHeader(PRAGMA)).thenReturn(null);
        when(request.getHeader(CACHE_CONTROL)).thenReturn(null);
        when(request.getHeader(USER_AGENT)).thenReturn(null);

        autoReloadFilter.doFilter(request, response, chain);

        verifyZeroInteractions(reloadHandler);
    }

    @Test
    public void assertThatReloadIsNotTriggeredByNonServletRequest() throws IOException, ServletException
    {
        when(request.getRequestURI()).thenReturn(NON_SERVLET_URI);
        autoReloadFilter.doFilter(request, response, chain);

        verifyZeroInteractions(reloadHandler);
    }

    @Test
    public void assertThatReloadIsNotTriggeredByPostRequest() throws IOException, ServletException
    {
        when(request.getMethod()).thenReturn(POST);
        autoReloadFilter.doFilter(request, response, chain);

        verifyZeroInteractions(reloadHandler);
    }

}
