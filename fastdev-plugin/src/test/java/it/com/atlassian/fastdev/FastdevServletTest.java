package it.com.atlassian.fastdev;

import java.net.URI;

import com.atlassian.integrationtesting.runner.TestGroupRunner;

import org.apache.wink.client.ClientConfig;
import org.apache.wink.client.ClientResponse;
import org.apache.wink.client.RestClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.integrationtesting.ApplicationPropertiesImpl.getStandardApplicationProperties;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@RunWith(TestGroupRunner.class)
public class FastdevServletTest
{
    private final URI fastdevUri;
    private RestClient client;

    public FastdevServletTest()
    {
        fastdevUri = URI.create(getStandardApplicationProperties().getBaseUrl() + "/plugins/servlet/fastdev");
    }

    @Before
    public void setup()
    {
        ClientConfig config = new ClientConfig();
        config.followRedirects(false);
        client = new RestClient(config);
    }

    @Test
    public void assertThatFastdevIsInstalled()
    {
        ClientResponse response = client.resource(fastdevUri).get();
        assertThat(response.getStatusCode(), is(equalTo(200)));
    }
}
