(function() {

    function getQueryParamByName(name){
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.search);
        if(results == null)
            return false;
        else
            return decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    var baseUrl,
        numTasks,
        $contentContainer,
        isFastdevServlet = window.location.pathname.indexOf('plugins/servlet/fastdev') > -1,
        hasFailure,
        redirectUrl = getQueryParamByName('origUrl');

    function buildTaskElement(task) {
        var data,
            root = task.buildRoot,
            name = root.substring(root.lastIndexOf('/') + 1);

        data = [
            {
                name: 'task-status',
                attr: {
                    id: 'task-status-' + task.id
                },
                text: Fastdev.util.params['fastdev.installing']
            },
            {
                name: 'maven-output',
                attr: {
                    id: 'fastdev-task-' + task.id
                }
            },
            {
                name: 'expand-button',
                attr: {
                    id: 'expand-' + task.id
                }
            },
            {
                name: 'task-root',
                text: root
            },
            {
                name: 'plugin-name',
                text: name,
                attr: {
                    title: Fastdev.util.format(Fastdev.util.params['fastdev.building.from'], root)
                }
            },
            {
                name: 'task-progress',
                attr: {
                    id: 'task-progress-' + task.id
                }
            }
        ];

        return Fastdev.template.fill('task', data);
    }

    function disableTriggerButton() {
        return $contentContainer.find('button.trigger-reload')
            .attr('disabled', 'disabled')
            .addClass('disabled');
    }

    function enableTriggerButton() {
        return $contentContainer.find('button.trigger-reload')
                    .removeAttr('disabled')
                    .removeClass('disabled');
    }

    function handleTaskCollection(taskCollection) {
        var $taskContainer = $('#fastdev-tasks');

        numTasks = taskCollection.tasks.length;

        $contentContainer.removeClass('loading');

        if (!numTasks) {
            $contentContainer.removeClass('has-tasks').addClass('no-changes');
            enableTriggerButton();
            return;
        }

        setReloadingPluginsMsg();
        $contentContainer.removeClass('no-changes').addClass('has-tasks');

        $taskContainer.find('div.task').remove();

        // Create a div area for each task
        $.each(taskCollection.tasks, function(index, task) {
            buildTaskElement(task).appendTo($taskContainer);
            updateTaskOutput(task);
        });
    }

    // Initial call to get task summaries
    function getMavenTasks() {
        $.ajax({
            url: baseUrl + '/rest/fastdev/1.0/tasks/',
            dataType: 'json',
            cache: false,
            success: handleTaskCollection
        });
    }

    // Expand / collapse maven output divs
    function expandCollapse(evt) {
        var $target = $(evt.target),
            isExpanded;
        if ($target.hasClass('expand')) {
            isExpanded = $target.hasClass('open');
            $target.toggleClass('open');
            $target.toggleClass('closed');

            $target.closest('div.task').find('div.maven-output')
                .animate({
                             height: isExpanded ? '150px' : '600px'
                         },
                         function() {
                             $target.text(isExpanded ? Fastdev.util.params['fastdev.expand'] : Fastdev.util.params['fastdev.collapse']);

                         });
            evt.preventDefault();
        }
    }

    function resize($element, height) {
        return $element
            .height(height)
            .attr({scrollTop: $element.attr('scrollHeight')});
    }

    function resizeOutput(evt) {
        var $doc = $(document),
            $container = evt.data.$container,
            $guide = evt.data.$guide,
            $output = $container.find('div.maven-output'),
            originalY = evt.data.originalY;
        
        $guide.remove();
        
        $doc
            .unbind(evt)
            .unbind('mousemove', handleMousemove);

        resize($output, $output.height() + evt.pageY - originalY);
    }

    function handleMousemove(evt) {
        var y = evt.pageY,
            $guide = evt.data.$guide;
        $guide
            .height(y - $guide.offset().top)
            // This is a colossal hack to make the guide show up in the correct spot in IE9.  But it causes the scrollbar
            // to jump slightly when first resizing in that browser.
            .css("position", "absolute");
    }

    function startResize(evt) {
        var $target = $(evt.target),
            $container = $target.closest('div.maven-output-container'),
            $guide = $('<div></div>').addClass('resize-guide'),
            $doc = $(document),
            originalY = evt.pageY;

        $guide
            .height($container.height())
            .width($container.width())
            .offset($container.offset())
            .appendTo('body');

        $doc
            .bind('mousemove', {$guide: $guide}, handleMousemove)
            .bind('mouseup', {$container: $container, $guide: $guide, originalY: originalY}, resizeOutput);
        evt.preventDefault();
    }

    // Change the title of the page
    function setReloadingPluginsMsg() {
        var msg;
        if (!numTasks) {
            msg = Fastdev.util.params['fastdev.reloading.plugins.none'];
        } else if (numTasks === 1) {
            msg = Fastdev.util.params['fastdev.reloading.plugins.singular'];
        } else {
            msg = Fastdev.util.format(Fastdev.util.params['fastdev.reloading.plugins'], numTasks);
        }
        $('#fastdev-tasks-header').text(msg);
        window.title = msg;
    }

    function updateProgressBar(task, $container) {
        var avg = parseInt(task.averageTaskTime),
            elapsed = parseInt(task.elapsedTime),
            msg = '';
        // Update the progress bar if we have a known average task time for this build
        if (avg && avg !== NaN) {
            var diff = Math.round((avg - elapsed) / 1000),
                $progress = $('#task-progress-' + task.id);

            $container.addClass('has-progress');

            if (diff < 0) {
                diff = -diff;
                msg = diff == 1 ?
                      Fastdev.util.params['fastdev.time.slower.singular'] :
                      Fastdev.util.format(Fastdev.util.params['fastdev.time.slower'], diff);
            } else {
                msg = diff == 1 ?
                      Fastdev.util.params['fastdev.time.remaining.singular'] :
                      Fastdev.util.format(Fastdev.util.params['fastdev.time.remaining'], diff);
            }

            $progress.find('div.progress-text').text(msg);

            $progress.find('div.progress-bar').animate({
                                                           width: Math.min(((elapsed / avg) * 100), 100) + '%'
                                                       }, 400);
        }
    }

    function getClassNameForOutput(line) {
        // Add classes to the lines based on their contents
        if (!line.indexOf('[INFO]')) {
            return 'maven-info-output';
        } else if (!line.indexOf('[ERROR]')) {
            return 'maven-error-output';
        } else {
            return 'maven-other-output';
        }
    }

    function updateFinishedTask() {
        if (!--numTasks) {
            enableTriggerButton();
        }
        setReloadingPluginsMsg();
    }

    // Periodic call to update build output with data from the server
    function updateTaskOutput(task) {
        var $outputContainer = $('#fastdev-task-' + task.id),
            $parentContainer = $outputContainer.closest('div.task'),
            resizeHeight = 600,
            msg;

        // Server returned an error code (not build error)
        if (task.error) {
            hasFailure = true;
            msg = Fastdev.util.format(Fastdev.util.params['fastdev.error.server'], task.error);
            $('#task-status-' + task.id).text(msg);
            $parentContainer.addClass('server-error');
            updateFinishedTask();
            return;
        }

        // New maven output received
        if (task.output) {
            $outputContainer.empty();

            $.each(task.output, function(index, line) {
                var className = getClassNameForOutput(line);

                if (className === 'maven-error-output' && !$parentContainer.hasClass('failure')) {
                    $parentContainer.addClass('failure');
                    if ($outputContainer.height() < resizeHeight) {
                        resize($outputContainer, resizeHeight);
                    }
                }

                $outputContainer
                    .append($('<div></div>').text(line).attr('class', className))
                    .attr({scrollTop: $outputContainer.attr('scrollHeight')});

                if (line.indexOf('BUILD SUCCESSFUL') > 0) {
                    $parentContainer.addClass('successful');
                }
            });

            updateProgressBar(task, $parentContainer);
        }

        // The task hasn't yet completed
        if (task.exitCode === undefined) {
            setTimeout(function () {
                getTaskDetails(task, updateTaskOutput);
            }, 400);
        } else {
            $parentContainer.removeClass('has-progress');
            updateFinishedTask();

            // The task completed without an error, see how many are left
            if (task.exitCode === 0) {

                $parentContainer.addClass('successful');
                
                if (!numTasks && !isFastdevServlet && !hasFailure) {
                    // Done!
                    window.location.reload(true);
                } else if (!numTasks && !hasFailure && redirectUrl) {
                    // Added to support reload button from Dev Toolbox toolbar
                    window.location = redirectUrl;
                }

                msg = Fastdev.util.params['fastdev.install.success'];
            }
            // The task completed with an error. The page won't be reloaded, so that the error can be examined.
            else {
                hasFailure = true;
                msg = Fastdev.util.format(Fastdev.util.params['fastdev.install.failure'], task.exitCode);
            }

            $('#task-status-' + task.id).text(msg);

        }
    }

    function getTaskDetails(task, callback) {
        $.ajax({
            url: task.links['self'],
            dataType: 'json',
            cache: false,
            success: function(taskDetails) {
                callback(taskDetails);
            },
            // Server error, maybe from trying to reload unchanged java files
            error: function(response) {
                callback({
                             error: response.statusText ? response.status + ' ' + response.statusText : response.status,
                             id: task.id
                         });
            }
        });
    }

    function triggerReload(evt) {
        var $target = $(evt.target);
        if (!$target.hasClass('disabled')) {
            // disabled attribute does nothing in IE, so need to manually check
            $contentContainer.addClass('loading');
            hasFailure = false;
            disableTriggerButton();

            $.ajax({
                       url: baseUrl + '/rest/fastdev/1.0/reload',
                       type: 'post',
                       cache: false,
                       contentType: 'application/json',
                       dataType: 'json',
                       success: function(data) {
                           if (!data) {
                               // no tasks
                               data = {tasks: []};
                           }
                           handleTaskCollection(data);
                       },
                       data: 'data'
                   });
        }
    }

    $('.trigger-reload').live('click', triggerReload);
    $('div.handle').live('mousedown', startResize);

    $(document).ready(function() {
        // Change text to read "control + reload" instead of "shift + reload" for IE
        // Ideally this should be done in velocity instead of js, but I couldn't figure that out
        $.browser.msie && $('strong.fastdev-trigger-combo', '#fastdev-no-changes').text(Fastdev.util.params['fastdev.trigger.control.reload']);
        
        baseUrl = Fastdev.util.params['application-base-url'];
        $contentContainer = $('#content');
        getMavenTasks();
    });
})();
