package com.atlassian.fastdev.maven;

import java.io.*;
import java.net.Socket;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import com.atlassian.fastdev.ScanResult;
import com.atlassian.fastdev.util.AvailablePortFinder;
import com.atlassian.fastdev.util.StreamEater;
import com.atlassian.fastdev.util.StreamPromptFinder;

import com.google.common.collect.ImmutableList;

import static org.apache.commons.lang.SystemUtils.IS_OS_WINDOWS;

/**
 * @since version
 */
public class CLIMavenTask implements MavenTask
{
    public static final Integer DEFAULT_CLI_PORT = 4330;
    private static volatile Integer defaultPort = 0;
    
    private final MavenTaskManager mavenTaskManager;
    private final ExecutorService executorService;
    private final ScanResult scanResult;
    private final UUID uuid;
    private final ConcurrentLinkedQueue<String> output;
    private final List<String> cliArgs;
    private final String cliCommand;
    private volatile Integer exitCode;
    private final long currentAverage;
    private volatile long startTime;
    

    CLIMavenTask(UUID uuid, ScanResult result, List<String> cliArgs, String command, MavenTaskManager mavenTaskManager, ExecutorService executorService)
    {
        this.mavenTaskManager = mavenTaskManager;
        this.executorService = executorService;
        this.scanResult = result;
        this.cliArgs = cliArgs;
        this.cliCommand = command;
        this.uuid = uuid;
        this.output = new ConcurrentLinkedQueue<String>();

        this.currentAverage = mavenTaskManager.averageBuildTime(scanResult.getBuildRoot());
    }

    @Override
    public File getBuildRoot()
    {
        return scanResult.getBuildRoot();
    }

    @Override
    public long getAverageTaskTime()
    {
        return currentAverage;
    }

    @Override
    public UUID getUuid()
    {
        return uuid;
    }

    @Override
    public Iterable<String> getOutput()
    {
        return ImmutableList.copyOf(output);
    }

    @Override
    public Integer getExitCode()
    {
        return exitCode;
    }

    @Override
    public void setExitCode(Integer code)
    {
        this.exitCode = code;
    }

    @Override
    public long getElapsedTime()
    {
        return System.currentTimeMillis() - startTime;
    }

    @Override
    public long getStartTime()
    {
        return startTime;
    }

    @Override
    public Future<Integer> start()
    {
        return executorService.submit(new Callable<Integer>()
        {
            public Integer call() throws Exception
            {
                Integer port = 0;
                try
                {
                    startTime = System.currentTimeMillis();
                    port = CLIMavenTask.this.startCliIfNeeded(scanResult, cliArgs, output);

                    CLIProcess cliProcess = mavenTaskManager.getCliProcess(scanResult.getBuildRoot());
                    Process process = cliProcess.getProcess();

                    mavenTaskManager.LOG.info("sending command to CLI: " + cliCommand);

                    int cliSuccess = 0;

                    if (IS_OS_WINDOWS)
                    {
                        StreamEater eater = new StreamEater(process.getInputStream(),output);
                        cliSuccess = execWithSocket(port);

                        eater.kill();
                    }
                    else
                    {
                        cliSuccess = execWithProcess(process);
                    }

                    exitCode = cliSuccess;
                    mavenTaskManager.recordBuildTime(scanResult.getBuildRoot(), System.currentTimeMillis() - startTime);
                    mavenTaskManager.remove(CLIMavenTask.this);
                    
                    return exitCode;

                }
                catch (IOException e)
                {
                    mavenTaskManager.LOG.error("Unable to send data to port", e);
                    throw new Exception("Cannot connect to the cli port '" + port + "'.");
                }

            }
        });
    }

    private int execWithSocket(Integer port) throws Exception
    {
        mavenTaskManager.LOG.debug("Connecting to socket on port: " + port);
        Socket socket = new Socket("localhost",port);
        OutputStream out = socket.getOutputStream();
        InputStream is = socket.getInputStream();

        mavenTaskManager.LOG.debug("starting stream eater");
        
        StreamPromptFinder finder = new StreamPromptFinder(is,output,mavenTaskManager.LOG);
        
        mavenTaskManager.LOG.debug("writing command to socket: " + cliCommand);
        
        out.write((cliCommand + "\n").getBytes());
        out.flush();

        mavenTaskManager.LOG.debug("looking for maven cli prompt from socket...");
        
        finder.join();
        
        socket.close();
        is = null;
        out = null;
        socket = null;
        
        exitCode = outputContainsError();
        
        mavenTaskManager.LOG.debug("returning exit code");
        
        return exitCode;
    }

    private Integer outputContainsError()
    {
        int success = 0;
        for(String line : output)
        {
            if(line.contains("ERROR]"))
            {
                success = 1;
                break;
            }
        }

        if(success < 1)
        {
            output.add("BUILD SUCCESSFUL\n");
        }
        else
        {
            output.add("BUILD ERROR");
        }
        
        return success;
    }

    private int execWithProcess(Process process) throws Exception
    {
        OutputStream out = process.getOutputStream();
        out.write((cliCommand + "\n").getBytes());
        out.flush();

        InputStreamReader isr = new InputStreamReader(process.getInputStream());
        BufferedReader br = new BufferedReader(isr);
        return readUntilMavenCliPrompt(br, output);
    }
    
    private Integer pickFreeCliPort(Integer oldPort) throws Exception
    {
        if(CLIMavenTask.defaultPort < 1)
        {
            CLIMavenTask.defaultPort = DEFAULT_CLI_PORT;
        }
        else {
            CLIMavenTask.defaultPort = CLIMavenTask.defaultPort + 1;
        }
        
        Integer newDefaultPort = mavenTaskManager.cliPortTaken(CLIMavenTask.defaultPort) ? CLIMavenTask.defaultPort + 1 : CLIMavenTask.defaultPort;

        CLIMavenTask.defaultPort = newDefaultPort;
        
        if (oldPort > 0)
        {
            return AvailablePortFinder.getPortOrNextAvailable(oldPort, newDefaultPort);
        }
        else
        {
            return AvailablePortFinder.getPortOrNextAvailable(newDefaultPort, newDefaultPort);
        }
    }

    private synchronized Integer startCliIfNeeded(ScanResult result, List<String> cliArgs, ConcurrentLinkedQueue<String> output) throws Exception
    {
        Integer port = 0;

        if (!mavenTaskManager.hasCliProcess(result.getBuildRoot()) || result.isPomChanged())
        {
            CLIProcess cliProcess = mavenTaskManager.getCliProcess(result.getBuildRoot());
            Integer oldPort = 0;

            if (null != cliProcess)
            {
                oldPort = cliProcess.getPort();
                mavenTaskManager.removeCliProcess(result.getBuildRoot(), cliProcess);
                cliProcess.getProcess().destroy();
            }

            Integer newPort = pickFreeCliPort(oldPort);

            ImmutableList.Builder<String> commandsBuilder = ImmutableList.builder();
            if (IS_OS_WINDOWS)
            {
                commandsBuilder.add("cmd", "/c");
            }
            commandsBuilder.add(mavenTaskManager.getMavenCommand());
            commandsBuilder.addAll(cliArgs);
            commandsBuilder.add("-Dcli.port=" + newPort);

            List<String> commands = commandsBuilder.build();

            ProcessBuilder processBuilder = new ProcessBuilder(commands);
            // execute the process in the specified buildRoot
            processBuilder.directory(result.getBuildRoot());
            processBuilder.environment().remove("MAVEN_COLOR");

            Process process = processBuilder.start();
            mavenTaskManager.addCliProcess(result.getBuildRoot(), new CLIProcess(newPort, process));

            InputStreamReader isr = new InputStreamReader(process.getInputStream());
            BufferedReader br = new BufferedReader(isr);

            readUntilMavenCliPrompt(br, output);

            mavenTaskManager.LOG.info("Maven CLI started on port: " + newPort);
            port = newPort;
        }
        else
        {
            port = mavenTaskManager.getCliProcess(result.getBuildRoot()).getPort();
        }

        return port;

    }

    private int readUntilMavenCliPrompt(BufferedReader br, ConcurrentLinkedQueue<String> output) throws Exception
    {
        mavenTaskManager.LOG.debug("reading until prompt is found...");
        int exit = readUntilStringFound("maven>", br, output);
        if(exit < 1)
        {
            output.add("BUILD SUCCESSFUL\n");
        }
        else
        {
            output.add("BUILD ERROR");
        }
        
        mavenTaskManager.LOG.info("Got CLI prompt");
        
        return exit;
    }

    private int readUntilStringFound(String token, BufferedReader br, ConcurrentLinkedQueue<String> output) throws Exception
    {
        int exit = 0;
        
        int c;
        char[] endMarker = token.toCharArray();
        char[] endBuffer = new char[endMarker.length];
        int bufLen = 0;
        int markerEndIndex = endMarker.length - 1;
        boolean foundMarker = false;
        StringBuffer lineBuffer = new StringBuffer();

        while ((c = br.read()) > -1)
        {
            mavenTaskManager.LOG.debug("cli char: " + (char) c);
            System.out.print((char) c);
            lineBuffer.append((char) c);

            if ((c == '\n') || (c == '\r'))
            {
                String line = lineBuffer.toString();
                if(line.contains("ERROR]"))
                {
                    exit = 1;
                }
                
                output.add(line);
                mavenTaskManager.LOG.debug("cli: " + line);
                lineBuffer = new StringBuffer();
            }

            if (!foundMarker && c == endMarker[bufLen])
            {
                endBuffer[bufLen] = (char) c;

                if (bufLen == markerEndIndex)
                {
                    
                        String line2 = lineBuffer.toString();
                        foundMarker = true;
                        output.add(line2);
                        mavenTaskManager.LOG.debug("cli: " + line2);
                        break;

                }
                bufLen++;
            }
            else
            {
                bufLen = 0;
            }
        }

        if (!foundMarker)
        {
            throw new Exception("unable to get maven> prompt");
        }
        
        return exit;
    }

}
