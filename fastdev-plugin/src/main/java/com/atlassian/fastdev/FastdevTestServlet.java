package com.atlassian.fastdev;


import java.io.IOException;
import java.net.URI;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.templaterenderer.TemplateRenderer;

import com.google.common.collect.ImmutableMap;

public class FastdevTestServlet extends HttpServlet
{
    private final TemplateRenderer renderer;
    private final ApplicationProperties applicationProperties;
    private final FastdevProperties properties;

    public FastdevTestServlet(TemplateRenderer renderer, ApplicationProperties applicationProperties, FastdevProperties properties)
    {
        this.renderer = renderer;
        this.applicationProperties = applicationProperties;
        this.properties = properties;
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        response.setContentType("text/html;charset=utf-8");
        Map<String, Object> context = ImmutableMap.<String,Object>of("baseurl", URI.create(applicationProperties.getBaseUrl()).normalize().toASCIIString(), "version", properties.getFastdevVersion());
        renderer.render("fastdev-output.vm", context, response.getWriter());
    }
}
